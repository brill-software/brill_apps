# Media Library
Welcome to the Media Library! This dedicated space serves as the central hub for storing and managing media assets including images, photographs, audio, and video clips. 
The aim is to facilitate easy access and efficient collaboration across all CMS users.

## Shared Folder
### Location and Usage
#### Repository Storage: 
Files within the **shared** folder are not stored in the repository, in order to optimize space and improve accessibility. For important files that you want commited to the respository, please place them in a folder under the **MediaLibrary** folder 
but not under the **shared** folder.
#### Collaboration: 
The **shared** folder is accessible to all CMS users, making it an ideal spot for sharing assets among team members. It's designed to streamline the collaborative process, ensuring that all media assets are easily available to those who need them.
### Best Practices
#### Use within your projects:
Please copy any images that you use within your web application to the web applications **Images** folder. Copy icons to the **Icons** folder.
#### Organization:
Please keep files organized in appropriately named folders and add a README. Delete or archive old versions or use a version naming convention.
#### Content Guidelines: 
Please maintain a professional and respectful media environment. It is strictly prohibited to upload inappropriate content. This includes, but is not limited to, offensive, explicit, or derogatory material.
#### Copyright Compliance: 
It is crucial to respect intellectual property rights. Ensure that you have the appropriate permissions or licenses for any media you upload. Infringing on someone's copyright not only disrespects their work but also exposes you and the organization to legal repercussions.
## Support and Questions
For assistance with the Media Library or if you have any questions about acceptable use, please don't hesitate to reach out to the support team.

---

README written by ChatGPT 4.